### README for ViaBill Quickpay Payment module protocol 10 (version 2.0) ###
---------------------

ViaBill has developed a free payment module using Quickpay Payment Gateway which enables your customers to pay online for their orders in your Oscommerce Webshop.
ViaBill is a Payment Method and not a Payment Gateway.

###Facts###
----
- version: 1.0
- Plugin on BitBucket (https://bitbucket.org/ibilldev/viabill-quickpay10-oscommerce-plugin)


###Description###
-----------
Pay using ViaBill. 
Install this Plugin in to your OsCommerce Web Shop to provide a separate payment option to pay using ViaBill.

#REQUIREMENTS:
-----------
You must have the following:

- Oscommerce 2.3 based webshop (might also work with version 2.2)
- QuickPay Payment manager login (get it here: https://manage.quickpay.net).
- PHP version 5
- The php-extension Curl must be installed on your webserver and access to quickpay.net allowed by your web hosting provider (normally this is allowed) in order 	to use this module.
  



###Integration Instructions###
-------------------------
Step 1

 Download the Module from the bitbucket. 

Step 2 

new files:
copy these files and folders to your webshop


	
	vbcallback10.php
	includes/modules/payment/viabill_quickpay.php
    includes/languages/danish/modules/payment/viabill_quickpay.php	
	includes/languages/english/modules/payment/viabill_quickpay.php
	includes/classes/VBQPConnectorCurl.php
    includes/classes/VBQPConnectorFactory.php
    includes/classes/VBQPConnectorInterface.php
    includes/classes/ViaBillQuickpayApi.php
	images/icons/
	admin/vbquickpay10/application_top_vbquickpay.php
	admin/vbquickpay10/orders_actions.php
	admin/vbquickpay10/orders_gui_admin.php
	admin/vbquickpay10/orders_js.php
	admin/vbquickpay10/vbquickpay10.php
	

##################################################################################### 


Step 3

modified files:
make sure your apply all modifications - marked with "viabill quickpay..."

	checkout_process.php
	checkout_confirmation.php
	
	admin/orders.php
	admin/invoice.php
	admin/includes/classes/order.php
	admin/includes/classes/currencies.php
	admin/includes/languages/danish/orders.php
	admin/includes/languages/danish/invoice.php
	
	admin/includes/languages/english/orders.php
	admin/includes/languages/english/invoice.php
	includes/classes/order.php
	includes/classes/currencies.php
	includes/languages/danish.php
    includes/languages/english.php
    includes/modules/boxes/bm_information.php (adds payment icons)

#####################################################################################
  
#####################################################################################


STEP 4

Go into the Shop Administration->Modules->Payment.


Install the module "ViaBill QuickPay Payment Option".

#####################################################################################



##Uninstallation/Disable Module/Delete Module
-----------------------
1. Go to the Administration->Modules-> Payment
2. Look For "ViaBill QuickPay Payment Option"
3. Choose Option to Remove




#Support
-------
If you have any issues with this extension, kindly drop us a mail on [support@viabill.com](mailto:support@viabill.com)

#Contribution
------------


#License
-------
[OSL - Open Software Licence 3.0](http://opensource.org/licenses/osl-3.0.php)