<?php
/**
 * Quickpay v10+ php library
 * 
 * This interface must be implemented by any Quickpay connectors.
 */

interface VBQPConnectorInterface {
    
    public function request($data);
    
}
?>
