<?php
/**
 * Quickpay v10+ php library
 * 
 * Singleton for easy retrieval of the Quickpay connector implementation.
 */

class VBQPConnectorFactory {

    public static function getConnector() {
        static $inst = null;
        if ($inst === null) {
            $inst = new VBQPConnectorCurl();
        }
        return $inst;
    }

    private function __construct() {
    }
}
?>
