<?php
 include(DIR_FS_CATALOG.DIR_WS_CLASSES.'ViaBillQuickpayApi.php');

/*
  viabill_quickpay.php, v1.1 - 2016-03-02

   osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2008 osCommerce

  Released under the GNU General Public License
 */

class viabill_quickpay {

    var $code, $title, $description, $enabled, $creditcardgroup, $num_groups;

// class constructor
    function viabill_quickpay() {
        global $order,$cardlock;

        $this->code = 'viabill_quickpay';
        $this->title = MODULE_PAYMENT_VIABILL_QUICKPAY_TEXT_TITLE;
        $this->public_title = MODULE_PAYMENT_VIABILL_QUICKPAY_TEXT_PUBLIC_TITLE;
        $this->description = MODULE_PAYMENT_VIABILL_QUICKPAY_TEXT_DESCRIPTION;
        $this->sort_order = MODULE_PAYMENT_VIABILL_QUICKPAY_SORT_ORDER;
        $this->enabled = ((MODULE_PAYMENT_VIABILL_QUICKPAY_STATUS == 'True') ? true : false);
        $this->creditcardgroup = array();
        $this->email_footer = ( $cardlock == "viabill" ? DENUNCIATION : '');
        
        // CUSTOMIZE THIS SETTING FOR THE NUMBER OF PAYMENT GROUPS NEEDED
        $this->num_groups = 1;

        if ((int) MODULE_PAYMENT_VIABILL_QUICKPAY_PREPARE_ORDER_STATUS_ID > 0) {
            $this->order_status = MODULE_PAYMENT_VIABILL_QUICKPAY_PREPARE_ORDER_STATUS_ID;
        }

        if (is_object($order))
            $this->update_status;

 
        // Store online payment options in local variable
        //for ($i = 1; $i <= $this->num_groups; $i++) {
            if (defined('MODULE_PAYMENT_VIABILL_QUICKPAY_GROUP') && constant('MODULE_PAYMENT_VIABILL_QUICKPAY_GROUP' ) != '') {
              /*  if (!isset($this->creditcardgroup[constant('MODULE_PAYMENT_VIABILL_QUICKPAY_GROUP' . $i . '_FEE')])) {
                  $this->creditcardgroup[constant('MODULE_PAYMENT_VIABILL_QUICKPAY_GROUP' . $i . '_FEE')] = array();
                }*/
                $option = 'viabill';

                //foreach ($payment_options as $option) {
                   $msg = $option;
               //     $this->creditcardgroup[constant('MODULE_PAYMENT_VIABILL_QUICKPAY_GROUP' . $i . '_FEE')][] = $option;
                //}
           
           
            }
            
       // }
//V10       
           if($_POST['quickpayIT'] == "go" && !isset($_SESSION['qlink'])) { 
            $this->form_action_url = 'https://payment.quickpay.net/';
           }else{
            $this->form_action_url = tep_href_link(FILENAME_CHECKOUT_CONFIRMATION, '', 'SSL');
           }
   
    }

// class methods

    function update_status() {
        global $order, $vbquickpay_fee, $HTTP_POST_VARS, $vbqp_card;

        if (($this->enabled == true) && ((int) MODULE_PAYMENT_QUICKPAY_ZONE > 0)) {
            $check_flag = false;
            $check_query = tep_db_query("select zone_id from " . TABLE_ZONES_TO_GEO_ZONES . " where geo_zone_id = '" . MODULE_PAYMENT_QUICKPAY_ZONE . "' and zone_country_id = '" . $order->billing['country']['id'] . "' order by zone_id");
            while ($check = tep_db_fetch_array($check_query)) {
                if ($check['zone_id'] < 1) {
                    $check_flag = true;
                    break;
                } elseif ($check['zone_id'] == $order->billing['zone_id']) {
                    $check_flag = true;
                    break;
                }
            }

            if ($check_flag == false) {
                $this->enabled = false;
            }
        }

        if (!tep_session_is_registered('vbqp_card'))
            tep_session_register('vbqp_card');
        if (isset($_POST['vbqp_card']))
            $vbqp_card = $_POST['vbqp_card'];
 
 if (!tep_session_is_registered('cart_VBQuickPay_ID'))
            tep_session_register('cart_VBQuickPay_ID');
        if (isset($_GET['cart_VBQuickPay_ID']))
            $vbqp_card = $_GET['cart_VBQuickPay_ID'];
            

        if (!tep_session_is_registered('vbquickpay_fee')) {
            tep_session_register('vbquickpay_fee');
        }
    }

    function javascript_validation() {


      $js = '  if (payment_value == "' . $this->code . '") {' . "\n" .
                '     var vbqp_card_value = null;' . "\n" .
                '      if (document.checkout_payment.vbqp_card.length) {' . "\n" .
                '        for (var i=0; i<document.checkout_payment.vbqp_card.length; i++) {' . "\n" .
                '          if (document.checkout_payment.vbqp_card[i].checked) {' . "\n" .
                '            vbqp_card_value = document.checkout_payment.vbqp_card[i].value;' . "\n" .
                '          }' . "\n" .
                '        }' . "\n" .
                '      } else if (document.checkout_payment.vbqp_card.checked) {' . "\n" .
                '        vbqp_card_value = document.checkout_payment.vbqp_card.value;' . "\n" .
                '      } else if (document.checkout_payment.vbqp_card.value) {' . "\n" .
                '        vbqp_card_value = document.checkout_payment.vbqp_card.value;' . "\n" .
                '        document.checkout_payment.vbqp_card.checked=true;' . "\n" .
                '      }' . "\n" .
                '    if (vbqp_card_value == null) {' . "\n" .
                '      error_message = error_message + "' . MODULE_PAYMENT_VIABILL_QUICKPAY_TEXT_SELECT_CARD . '";' . "\n" .
                '      error = 1;' . "\n" .
                '    }' . "\n" .
                ' if (document.checkout_payment.cardlock.value == null) {' . "\n" .
                '      error_message = error_message + "' . MODULE_PAYMENT_VIABILL_QUICKPAY_TEXT_SELECT_CARD . '";' . "\n" .
                '      error = 1;' . "\n" .
                '    }' . "\n" .
                '  }' . "\n";
        return $js;
        
        
        
    }

 function selection() {
        global $order, $currencies, $vbqp_card,$cardlock;
        $qty_groups = 1;
        //$fees =array();
         $option = constant('MODULE_PAYMENT_VIABILL_QUICKPAY_GROUP' );
        
       
/* --- This piece of code will display only Default Text of Viabill quickpay with no Icon
 if($qty_groups==1) { /
     $selection = array('id' => $this->code,
         'module' => $this->title. tep_draw_hidden_field('cardlock', $cardlock ));
    // $selection['module'] .= tep_draw_hidden_field('vbqp_card', (isset($fees[1])) ? $fees[1] : '0');
     
     }*/

if($qty_groups==1){
     if($option == "viabill"){
                  //upload images to images/icons corresponding to your chosen cardlock groups in your payment module settings
                  //OPTIONAL image if different from cardlogo, add _payment to filename
            
         //echo  $option; 
                $icon ="";
              //  foreach($selectedopts as $option){
               // $optscount++;
                
$icon = (file_exists(DIR_WS_ICONS.$option.".png") ? DIR_WS_ICONS.$option.".png": $icon);
$icon = (file_exists(DIR_WS_ICONS.$option.".jpg") ? DIR_WS_ICONS.$option.".jpg": $icon);
$icon = (file_exists(DIR_WS_ICONS.$option.".gif") ? DIR_WS_ICONS.$option.".gif": $icon);   
$icon = (file_exists(DIR_WS_ICONS.$option."_payment.png") ? DIR_WS_ICONS.$option."_payment.png": $icon);
$icon = (file_exists(DIR_WS_ICONS.$option."_payment.jpg") ? DIR_WS_ICONS.$option."_payment.jpg": $icon);
$icon = (file_exists(DIR_WS_ICONS.$option."_payment.gif") ? DIR_WS_ICONS.$option."_payment.gif": $icon);                   
        $space =0;
        //define payment icon width
        if(strstr($icon, "_payment")){
            $w='';//120;
            $h= 27;
            
            
        }else{
               $w= 35;
               $h= 22;
               
            
        }
        /* Price Tag Settings Start*/
        $ordertotal = $order->info["total"] ;
        $this->pricetagscript  = MODULE_PAYMENT_VIABILL_QUICKPAY_PRICETAGSRC;
        
         //$cost = $this->calculate_order_fee($order->info['total'], $fees[$i]);
         $options_text = '<table ><tr><td>'.tep_image($icon,$this->get_payment_options_name($option),$w,$h,' style="position:relative;border:0px;float:left;margin:'.$space.'px;" ').'</td><td style="height: 27px;white-space:nowrap;vertical-align:middle;font-size: 18px;font-color:#666;" >'.$this->get_payment_options_name($option).'</td></tr></table>';

//$ordertotal = 20;

          if($ordertotal && $this->pricetagscript){
            ?>
            <style>
            .lineheight{
                line-height: 7px;
            }
            </style>
            <script type="text/javascript">
                <?php echo $this->pricetagscript ?>
                var ordertotal  = <?php echo $ordertotal ?>;
                setTimeout(function(){
                   // alert( vb.isLow(ordertotal) );
                    if(vb.isLow(ordertotal)  || vb.isHigh(ordertotal)){}
                        else{
                            //alert("nskfdkjfkdj");
                            //document.getElementById('defaulttext').
                            
                            document.getElementById('defaulttext').innerHTML = '<?php echo $options_text; ?>';
                            $('#defaulttext').addClass( 'lineheight' );
                        }

                }, 800);
            </script>
            <?php
            
         }

 /* Price Tag Settings End*/


         $selection = array('id' => $this->code,
         'module' => '<table width="100%" border="0">
                    <tr class="moduleRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="selectVBQuickPayRowEffect(this, ' . ($optscount-1) . ',\''.$option.'\')">
                        <td class="main"  style="height: 27px;white-space:nowrap;vertical-align:middle;font-size: 18px;font-color:#666;"><div id="defaulttext" style="line-height: 7px; "><div class="ViaBill_pricetag_payment" price="'.$ordertotal.'">' .$options_text.($cost !=0 ? '</td><td style="height:22px;vertical-align:middle;"> (+ '.MODULE_PAYMENT_VIABILL_QUICKPAY_FEELOCKINFO.')' :'').'
                           </div> </div></td>
                    </tr></table>'.tep_draw_hidden_field('cardlock', $option).tep_draw_hidden_field('vbqp_card', (isset($fees[1])) ? $fees[1] : '0'));




         
    }//end of option check
}//end of if


   
            $js_function = '
        <script language="javascript"><!-- 
     
          function setVBQuickPay() {
            
              
            var radioLength = document.checkout_payment.payment.length;
            
            for(var i = 0; i < radioLength; i++) {
                
                document.checkout_payment.payment[i].checked = false;
                if(document.checkout_payment.payment[i].value == "viabill_quickpay") {
                
                    document.checkout_payment.payment[i].checked = true;
                    
                }
            }
          }
          function selectVBQuickPayRowEffect(object, buttonSelect, option) {
            if (!selected) {
              if (document.getElementById) {
                selected = document.getElementById("defaultSelected");
              } else {
                selected = document.all["defaultSelected"];
              }
            }
         
            if (selected) selected.className = "moduleRow";
            object.className = "moduleRowSelected";
            selected = object;
            document.checkout_payment.cardlock.value = option;
            document.checkout_payment.vbqp_card.checked = false;
              if (document.checkout_payment.vbqp_card[0]) {
              document.checkout_payment.vbqp_card[buttonSelect].checked=true;
            } else {
              document.checkout_payment.vbqp_card.checked=true;
            }
            setVBQuickPay();
          }
        //--></script>
        ';
            $selection['module'] .= $js_function;
      
    
        return $selection;
    }


    function pre_confirmation_check() {
        global $cartID, $cart;

        if (empty($cart->cartID)) {
            $cartID = $cart->cartID = $cart->generate_cart_id();
        }

        if (!tep_session_is_registered('cartID')) {
            tep_session_register('cartID');
        }
        $this->get_order_fee();
    }

    function confirmation($addorder=false) {
        global $cartID, $cart_VBQuickPay_ID, $customer_id, $languages_id, $order, $order_total_modules;
$order_id = substr($cart_VBQuickPay_ID, strpos($cart_VBQuickPay_ID, '-') + 1);
//do not create preparing order id before payment confirmation is chosen by customer

$mode = false;
if(MODULE_PAYMENT_VIABILL_QUICKPAY_MODE == "Before" || $_POST['callvbquickpay'] == "go"){
    
    $mode = true;
}

if($mode && !$order_id) {


  
// write new pro forma order if payment link is not created
//if(!isset($_SESSION['qlink'])){
                $order_totals = array();
                if (is_array($order_total_modules->modules)) {
                    reset($order_total_modules->modules);
                    while (list(, $value) = each($order_total_modules->modules)) {
                        $class = substr($value, 0, strrpos($value, '.'));
                        if ($GLOBALS[$class]->enabled) {
                            for ($i = 0, $n = sizeof($GLOBALS[$class]->output); $i < $n; $i++) {
                                if (tep_not_null($GLOBALS[$class]->output[$i]['title']) && tep_not_null($GLOBALS[$class]->output[$i]['text'])) {
                                    $order_totals[] = array('code' => $GLOBALS[$class]->code,
                                        'title' => $GLOBALS[$class]->output[$i]['title'],
                                        'text' => $GLOBALS[$class]->output[$i]['text'],
                                        'value' => $GLOBALS[$class]->output[$i]['value'],
                                        'sort_order' => $GLOBALS[$class]->sort_order);
                                }
                            }
                        }
                    }
                }

                $sql_data_array = array('customers_id' => $customer_id,
                    'customers_name' => $order->customer['firstname'] . ' ' . $order->customer['lastname'],
                    'customers_company' => $order->customer['company'],
                    'customers_street_address' => $order->customer['street_address'],
                    'customers_suburb' => $order->customer['suburb'],
                    'customers_city' => $order->customer['city'],
                    'customers_postcode' => $order->customer['postcode'],
                    'customers_state' => $order->customer['state'],
                    'customers_country' => $order->customer['country']['title'],
                    'customers_telephone' => $order->customer['telephone'],
                    'customers_email_address' => $order->customer['email_address'],
                    'customers_address_format_id' => $order->customer['format_id'],
                    'delivery_name' => $order->delivery['firstname'] . ' ' . $order->delivery['lastname'],
                    'delivery_company' => $order->delivery['company'],
                    'delivery_street_address' => $order->delivery['street_address'],
                    'delivery_suburb' => $order->delivery['suburb'],
                    'delivery_city' => $order->delivery['city'],
                    'delivery_postcode' => $order->delivery['postcode'],
                    'delivery_state' => $order->delivery['state'],
                    'delivery_country' => $order->delivery['country']['title'],
                    'delivery_address_format_id' => $order->delivery['format_id'],
                    'billing_name' => $order->billing['firstname'] . ' ' . $order->billing['lastname'],
                    'billing_company' => $order->billing['company'],
                    'billing_street_address' => $order->billing['street_address'],
                    'billing_suburb' => $order->billing['suburb'],
                    'billing_city' => $order->billing['city'],
                    'billing_postcode' => $order->billing['postcode'],
                    'billing_state' => $order->billing['state'],
                    'billing_country' => $order->billing['country']['title'],
                    'billing_address_format_id' => $order->billing['format_id'],
                    'payment_method' => $order->info['payment_method'],
                    'cc_type' => $order->info['cc_type'],
                    'cc_owner' => $order->info['cc_owner'],
                    'cc_number' => $order->info['cc_number'],
                    'cc_expires' => $order->info['cc_expires'],
                    'cc_cardhash' => '',
                    'date_purchased' => 'now()',
                    'orders_status' => $order->info['order_status'],
                    'currency' => $order->info['currency'],
                    'currency_value' => $order->info['currency_value']);

     tep_db_perform(TABLE_ORDERS, $sql_data_array);

                $insert_id = tep_db_insert_id();
            
            
                for ($i = 0, $n = sizeof($order_totals); $i < $n; $i++) {
                    $sql_data_array = array('orders_id' => $insert_id,
                        'title' => $order_totals[$i]['title'],
                        'text' => $order_totals[$i]['text'],
                        'value' => $order_totals[$i]['value'],
                        'class' => $order_totals[$i]['code'],
                        'sort_order' => $order_totals[$i]['sort_order']);

     tep_db_perform(TABLE_ORDERS_TOTAL, $sql_data_array);

   
                }

// checkout_process section
// this section is taken from checkout_process.php
// update of stock data is done in checkout_process
// adapt if you have done adoptions there
// 
// the data of the order-obj is depending if it was created from the cart or
// by a given order-id
// therefore the product data needs to be written in the cart context
// 
// 




                for ($i = 0, $n = sizeof($order->products); $i < $n; $i++) {
                    /*
                     * do not update stock - until order is confirmed
                     * will be done in checkout_process                   
                     */
                    $sql_data_array = array('orders_id' => $insert_id,
                        'products_id' => tep_get_prid($order->products[$i]['id']),
                        'products_model' => $order->products[$i]['model'],
                        'products_name' => $order->products[$i]['name'],
                        'products_price' => $order->products[$i]['price'],
                        'final_price' => $order->products[$i]['final_price'],
                        'products_tax' => $order->products[$i]['tax'],
                        'products_quantity' => $order->products[$i]['qty']);
                    tep_db_perform(TABLE_ORDERS_PRODUCTS, $sql_data_array);
                    $order_products_id = tep_db_insert_id();

//------insert customer choosen option to order--------
                    $attributes_exist = '0';
                    $products_ordered_attributes = '';
                    if (isset($order->products[$i]['attributes'])) {
                        $attributes_exist = '1';
                        for ($j = 0, $n2 = sizeof($order->products[$i]['attributes']); $j < $n2; $j++) {
                            if (DOWNLOAD_ENABLED == 'true') {
                                $attributes_query = "select popt.products_options_name, poval.products_options_values_name, pa.options_values_price, pa.price_prefix, pad.products_attributes_maxdays, pad.products_attributes_maxcount , pad.products_attributes_filename 
                               from " . TABLE_PRODUCTS_OPTIONS . " popt, " . TABLE_PRODUCTS_OPTIONS_VALUES . " poval, " . TABLE_PRODUCTS_ATTRIBUTES . " pa 
                               left join " . TABLE_PRODUCTS_ATTRIBUTES_DOWNLOAD . " pad
                                on pa.products_attributes_id=pad.products_attributes_id
                               where pa.products_id = '" . $order->products[$i]['id'] . "' 
                                and pa.options_id = '" . $order->products[$i]['attributes'][$j]['option_id'] . "' 
                                and pa.options_id = popt.products_options_id 
                                and pa.options_values_id = '" . $order->products[$i]['attributes'][$j]['value_id'] . "' 
                                and pa.options_values_id = poval.products_options_values_id 
                                and popt.language_id = '" . $languages_id . "' 
                                and poval.language_id = '" . $languages_id . "'";
                                $attributes = tep_db_query($attributes_query);
                            } else {
                                $attributes = tep_db_query("select popt.products_options_name, poval.products_options_values_name, pa.options_values_price, pa.price_prefix from " . TABLE_PRODUCTS_OPTIONS . " popt, " . TABLE_PRODUCTS_OPTIONS_VALUES . " poval, " . TABLE_PRODUCTS_ATTRIBUTES . " pa where pa.products_id = '" . $order->products[$i]['id'] . "' and pa.options_id = '" . $order->products[$i]['attributes'][$j]['option_id'] . "' and pa.options_id = popt.products_options_id and pa.options_values_id = '" . $order->products[$i]['attributes'][$j]['value_id'] . "' and pa.options_values_id = poval.products_options_values_id and popt.language_id = '" . $languages_id . "' and poval.language_id = '" . $languages_id . "'");
                            }
                            $attributes_values = tep_db_fetch_array($attributes);

                            $sql_data_array = array('orders_id' => $insert_id,
                                'orders_products_id' => $order_products_id,
                                'products_options' => $attributes_values['products_options_name'],
                                'products_options_values' => $attributes_values['products_options_values_name'],
                                'options_values_price' => $attributes_values['options_values_price'],
                                'price_prefix' => $attributes_values['price_prefix']);
                            tep_db_perform(TABLE_ORDERS_PRODUCTS_ATTRIBUTES, $sql_data_array);

                            if ((DOWNLOAD_ENABLED == 'true') && isset($attributes_values['products_attributes_filename']) && tep_not_null($attributes_values['products_attributes_filename'])) {
                                $sql_data_array = array('orders_id' => $insert_id,
                                    'orders_products_id' => $order_products_id,
                                    'orders_products_filename' => $attributes_values['products_attributes_filename'],
                                    'download_maxdays' => $attributes_values['products_attributes_maxdays'],
                                    'download_count' => $attributes_values['products_attributes_maxcount']);
                                tep_db_perform(TABLE_ORDERS_PRODUCTS_DOWNLOAD, $sql_data_array);
                            }
                            $products_ordered_attributes .= "\n\t" . $attributes_values['products_options_name'] . ' ' . $attributes_values['products_options_values_name'];
                        }
                    }

                }
/// end checkout_process section
                $cart_VBQuickPay_ID = $cartID . '-' . $insert_id;
                tep_session_register('cart_VBQuickPay_ID');

   
//end if($_POST['quickpayIT'] == "go") {
}
if($this->email_footer !='' && $addorder==false){
        return array('title' => $this->email_footer);
    }else{return false;}
  //}   
    }

    function process_button() {
                global $_POST, $customer_id, $order, $currencies, $currency, $languages_id, $language, $cart_VBQuickPay_ID, $order_total_modules;
        /*
         * collect all post fields and attach as hiddenfieds to button
         */


        $process_button_string = '';
        $process_fields ='';
        $process_parameters = array();

        $qp_merchant_id = MODULE_PAYMENT_VIABILL_QUICKPAY_MERCHANTID;
        $qp_aggreement_id = MODULE_PAYMENT_VIABILL_QUICKPAY_AGGREEMENTID;


// TODO: dynamic language switching instead of hardcoded mapping
        $qp_language = "da";
        switch ($language) {
            case "english": $qp_language = "en";
                break;
            case "swedish": $qp_language = "se";
                break;
            case "norwegian": $qp_language = "no";
                break;
            case "german": $qp_language = "de";
                break;
            case "french": $qp_language = "fr";
                break;
        }
         $qp_branding_id = "";

         $qp_subscription = (MODULE_PAYMENT_VIABILL_QUICKPAY_SUBSCRIPTION == "Normal" ? "" : "1");
         $qp_cardtypelock = $_POST['cardlock'];
         $qp_autofee = (MODULE_PAYMENT_VIABILL_QUICKPAY_AUTOFEE == "No" || $qp_cardtypelock == 'viabill' ? "0" : "1");
         $qp_description = "Merchant ".$qp_merchant_id." ".(MODULE_PAYMENT_VIABILL_QUICKPAY_SUBSCRIPTION == "Normal" ? "Authorize" : "Subscription");
         $order_id = substr($cart_VBQuickPay_ID, strpos($cart_VBQuickPay_ID, '-') + 1);
         $qp_order_id = MODULE_PAYMENT_VIABILL_QUICKPAY_ORDERPREFIX.sprintf('%04d', $order_id);
// Calculate the total order amount for the order (the same way as in checkout_process.php)
        $qp_order_amount = 100 * $currencies->calculate($order->info['total'], true, $order->info['currency'], $order->info['currency_value'], '.', '');
        $qp_currency_code = $order->info['currency'];
        $qp_continueurl = tep_href_link(FILENAME_CHECKOUT_PROCESS, 'cart_VBQuickPay_ID='.$cart_VBQuickPay_ID, 'SSL');
        $qp_cancelurl = tep_href_link(FILENAME_CHECKOUT_PAYMENT, 'payment_error=' . $this->code, 'SSL');
        $qp_callbackurl = tep_href_link('vbcallback10.php','oid='.$order_id,'SSL');
        $qp_autocapture = (MODULE_PAYMENT_VIABILL_QUICKPAY_AUTOCAPTURE == "No" ? "0" : "1");
        $qp_version ="v10";
        $qp_apikey = MODULE_PAYMENT_VIABILL_QUICKPAY_APIKEY;

            $qp_product_id = "P03";
            //$qp_category = MODULE_PAYMENT_VIABILL_QUICKPAY_PAII_CAT;
            $qp_reference_title = $qp_order_id;
            $qp_vat_amount = ($order->info['tax'] ? $order->info['tax'] : "0.00");

  //custom vars
       $varsvalues = array('variables[customers_id]' => $customer_id,
                    'variables[customers_name]' => $order->customer['firstname'] . ' ' . $order->customer['lastname'],
                    'variables[customers_company]' => $order->customer['company'],
                    'variables[customers_street_address]' => $order->customer['street_address'],
                    'variables[customers_suburb]' => $order->customer['suburb'],
                    'variables[customers_city]' => $order->customer['city'],
                    'variables[customers_postcode]' => $order->customer['postcode'],
                    'variables[customers_state]' => $order->customer['state'],
                    'variables[customers_country]' => $order->customer['country']['title'],
                    'variables[customers_telephone]' => $order->customer['telephone'],
                    'variables[customers_email_address]' => $order->customer['email_address'],
                    'variables[delivery_name]' => $order->delivery['firstname'] . ' ' . $order->delivery['lastname'],
                    'variables[delivery_company]' => $order->delivery['company'],
                    'variables[delivery_street_address]' => $order->delivery['street_address'],
                    'variables[delivery_suburb]' => $order->delivery['suburb'],
                    'variables[delivery_city]' => $order->delivery['city'],
                    'variables[delivery_postcode]' => $order->delivery['postcode'],
                    'variables[delivery_state]' => $order->delivery['state'],
                    'variables[delivery_country]' => $order->delivery['country']['title'],
                    'variables[delivery_address_format_id]' => $order->delivery['format_id'],
                    'variables[billing_name]' => $order->billing['firstname'] . ' ' . $order->billing['lastname'],
                    'variables[billing_company]' => $order->billing['company'],
                    'variables[billing_street_address]' => $order->billing['street_address'],
                    'variables[billing_suburb]' => $order->billing['suburb'],
                    'variables[billing_city]' => $order->billing['city'],
                    'variables[billing_postcode]' => $order->billing['postcode'],
                    'variables[billing_state]' => $order->billing['state'],
                    'variables[billing_country]' => $order->billing['country']['title']);



                for ($i = 0, $n = sizeof($order->products); $i < $n; $i++) {
    
                    $order_products_id = tep_get_prid($order->products[$i]['id']);

//------insert customer choosen option to order--------
                    $attributes_exist = '0';
                    $products_ordered_attributes = '';
                    if (isset($order->products[$i]['attributes'])) {
                        $attributes_exist = '1';
                        for ($j = 0, $n2 = sizeof($order->products[$i]['attributes']); $j < $n2; $j++) {
                            if (DOWNLOAD_ENABLED == 'true') {
                                $attributes_query = "select popt.products_options_name, poval.products_options_values_name, pa.options_values_price, pa.price_prefix, pad.products_attributes_maxdays, pad.products_attributes_maxcount , pad.products_attributes_filename 
                               from " . TABLE_PRODUCTS_OPTIONS . " popt, " . TABLE_PRODUCTS_OPTIONS_VALUES . " poval, " . TABLE_PRODUCTS_ATTRIBUTES . " pa 
                               left join " . TABLE_PRODUCTS_ATTRIBUTES_DOWNLOAD . " pad
                                on pa.products_attributes_id=pad.products_attributes_id
                               where pa.products_id = '" . $order->products[$i]['id'] . "' 
                                and pa.options_id = '" . $order->products[$i]['attributes'][$j]['option_id'] . "' 
                                and pa.options_id = popt.products_options_id 
                                and pa.options_values_id = '" . $order->products[$i]['attributes'][$j]['value_id'] . "' 
                                and pa.options_values_id = poval.products_options_values_id 
                                and popt.language_id = '" . $languages_id . "' 
                                and poval.language_id = '" . $languages_id . "'";
                                $attributes = tep_db_query($attributes_query);
                            } else {
                                $attributes = tep_db_query("select popt.products_options_name, poval.products_options_values_name, pa.options_values_price, pa.price_prefix from " . TABLE_PRODUCTS_OPTIONS . " popt, " . TABLE_PRODUCTS_OPTIONS_VALUES . " poval, " . TABLE_PRODUCTS_ATTRIBUTES . " pa where pa.products_id = '" . $order->products[$i]['id'] . "' and pa.options_id = '" . $order->products[$i]['attributes'][$j]['option_id'] . "' and pa.options_id = popt.products_options_id and pa.options_values_id = '" . $order->products[$i]['attributes'][$j]['value_id'] . "' and pa.options_values_id = poval.products_options_values_id and popt.language_id = '" . $languages_id . "' and poval.language_id = '" . $languages_id . "'");
                            }
                            $attributes_values = tep_db_fetch_array($attributes);
                            

                            if ((DOWNLOAD_ENABLED == 'true') && isset($attributes_values['products_attributes_filename']) && tep_not_null($attributes_values['products_attributes_filename'])) {
   
                               
                            }
                            $products_ordered_attributes .= "(" . $attributes_values['products_options_name'] . ' ' . $attributes_values['products_options_values_name'].") ";
                        }
                    }
//------insert customer choosen option eof ----
             
                      $total_weight += ( $order->products[$i]['qty'] * $order->products[$i]['weight']);
                      $total_tax += tep_calculate_tax($total_products_price, $products_tax) * $order->products[$i]['qty'];
                      $total_cost += $total_products_price;

                      $products_ordered[] = $order->products[$i]['qty'] . ' x ' . $order->products[$i]['name'] . ' (' . $order->products[$i]['model'] . ') = ' . $currencies->display_price($order->products[$i]['final_price'], $order->products[$i]['tax'], $order->products[$i]['qty']) . $products_ordered_attributes . "-";

                }
                $ps="";
               while (list ($key, $value) = each($products_ordered)) {
          $ps .= $value;
        }
        
        $varsvalues["variables[products]"] = html_entity_decode($ps);
        $varsvalues["variables[shopsystem]"] = "OsCommerce";
  

//end custom vars

// register fields to hand over

        $process_parameters = array(
                    'agreement_id'                 => $qp_aggreement_id,
                    'amount'                       => $qp_order_amount,
                    'autocapture'                  => $qp_autocapture,
                    'autofee'                      => $qp_autofee,
                    //'branding_id'                  => $qp_branding_id,
                    'callbackurl'                  => $qp_callbackurl,
                    'cancelurl'                    => $qp_cancelurl,
                    'continueurl'                  => $qp_continueurl,
                    'currency'                     => $qp_currency_code,
                    'description'                  => $qp_description,
                    'google_analytics_client_id'   => $qp_google_analytics_client_id,
                    'google_analytics_tracking_id' => $analytics_tracking_id,
                    'language'                     => $qp_language,
                    'merchant_id'                  => $qp_merchant_id,
                    'order_id'                     => $qp_order_id,
                    'payment_methods'              => $qp_cardtypelock,
                    'product_id'                   => $qp_product_id,
                    //'category'                     => $qp_category,
                    'reference_title'              => $qp_reference_title,
                    'vat_amount'                   => $qp_vat_amount,
                    'subscription'                 => $qp_subscription,
                    'version'                      => 'v10'
                        );
 
 
 
 
 $process_parameters = array_merge($process_parameters,$varsvalues);


        
if($_POST['callvbquickpay'] == "go") {
        $apiorder= new ViaBillQuickpayApi();
    $apiorder->setOptions(MODULE_PAYMENT_VIABILL_QUICKPAY_USERAPIKEY);
    //set status request mode
    $mode = (MODULE_PAYMENT_VIABILL_QUICKPAY_SUBSCRIPTION == "Normal" ? "" : "1");
        //been here before?
        $exists = $this->get_vbquickpay_order_status($order_id, $mode);
    
    $qid = $exists["qid"];
    //set to create/update mode
    $apiorder->mode = (MODULE_PAYMENT_VIABILL_QUICKPAY_SUBSCRIPTION == "Normal" ? "payments/" : "subscriptions/");
      if($exists["qid"] == null){

      //create new quickpay order   
      $storder = $apiorder->createorder($qp_order_id, $qp_currency_code, $process_parameters);
      $qid = $storder["id"];

      }else{
       $qid = $exists["qid"];
       }
            
        $storder = $apiorder->link($qid, $process_parameters);  
            $process_button_string .= "<script>
       //alert('qp ".$qp_order_id."-".$order_id."');
 window.location.replace('".$storder['url']."');
      </script>";

    $process_button_string .=  "<input type='hidden' value='go' name='callvbquickpay' />". "\n".
                "<input type='hidden' value='" . $_POST['cardlock'] . "' name='cardlock' />";
     
}
    $process_button_string .=  "<input type='hidden' value='go' name='callvbquickpay' />". "\n".
                "<input type='hidden' value='" . $_POST['cardlock'] . "' name='cardlock' />";

      return $process_button_string;

     
        
    }

    function before_process() {


// called in FILENAME_CHECKOUT_PROCESS
// check if order is approved by callback

        global $customer_id, $order, $order_id, $order_totals, $order_total_modules, $sendto, $billto, $languages_id, $payment, $currencies, $cart, $cart_VBQuickPay_ID;
   

        $order_id = substr($cart_VBQuickPay_ID, strpos($cart_VBQuickPay_ID, '-') + 1);
    
       $order_status_approved_id = (MODULE_PAYMENT_VIABILL_QUICKPAY_ORDER_STATUS_ID > 0 ? (int) MODULE_PAYMENT_VIABILL_QUICKPAY_ORDER_STATUS_ID : (int) DEFAULT_ORDERS_STATUS_ID);

    $mode = (MODULE_PAYMENT_VIABILL_QUICKPAY_SUBSCRIPTION == "Normal" ? "" : "1");
 $checkorderid = $this->get_vbquickpay_order_status($order_id, $mode);
 if($checkorderid["oid"] != $order_id){
      tep_redirect(tep_href_link(FILENAME_CHECKOUT_PAYMENT, 'payment_error=' . $this->code, 'SSL'));
     
 }

        // adopt order status of order object to "real" status
        $order->info['order_status'] = $order_status_approved_id;

        //for debugging with FireBug / FirePHP
        global $firephp;
        if (isset($firephp)) {
            $firephp->log($order_id, 'order_id');
        }

        // everything is fine... continue
        

        
        
    }

    function after_process() {
        tep_session_unregister('cardlock');
        tep_session_unregister('order_id');
        tep_session_unregister('vbquickpay_fee');
        tep_session_unregister('vbqp_card');
        tep_session_unregister('cart_VBQuickPay_ID');
        tep_session_unregister('qlink');
    }

    function get_error() {
    
        
        global $cart_VBQuickPay_ID, $order, $currencies;
     //   $order_id = substr($cart_QuickPay_ID, strpos($cart_QuickPay_ID, '-') + 1);;

            $error_desc = MODULE_PAYMENT_VIABILL_QUICKPAY_ERROR_CANCELLED;
        $error = array('title' => MODULE_PAYMENT_VIABILL_QUICKPAY_TEXT_ERROR,
            'error' => $error_desc);


//avoid order number already used: create a payment link if payment window was aborted
//for some reason
/*if(!$_SESSION["qlink"])   {
  try {

            $apiorder= new ViaBillQuickpayApi();
    $apiorder->setOptions(MODULE_PAYMENT_VIABILL_QUICKPAY_USERAPIKEY);
    //$api->mode = 'payments?currency='.$qp_currency_code.'&order_id='.
$qp_order_id = MODULE_PAYMENT_VIABILL_QUICKPAY_AGGREEMENTID."_".sprintf('%04d', $order_id);
$mode = (MODULE_PAYMENT_VIABILL_QUICKPAY_SUBSCRIPTION == "Normal" ? "" : "1");
$exists = $this->get_vbquickpay_order_status($order_id, $mode);

if($exists["qid"] == null){

//create new quickpay order 
$storder = $apiorder->createorder($qp_order_id, $order->info['currency']);
$qid = $storder["id"];
    
}else{
$qid = $exists["qid"];
}
//create or update link
$process_parameters = array(
"amount" => 100 * $currencies->calculate($order->info['total'], true, $order->info['currency'], $order->info['currency_value'], '.', ''),
"currency" => $order->info['currency']
);

$storder = $apiorder->link($qid, $process_parameters);
$_SESSION['qlink'] = $storder['url'];

  
  } catch (Exception $e) {
   $err .= 'QuickPay Status: ';
            // An error occured with the status request
    $err .= 'Problem: ' . $this->json_message_front($e->getMessage()) ;
         //  tep_redirect(tep_href_link(FILENAME_CHECKOUT_PAYMENT, 'payment_error=' . $this->code, 'SSL'));
   $error['error'] = $error['error'].' - '.$err;
 
  }
  
 
  }  
    */    
     return $error;
    
    }

    function output_error() {
        return false;
    }

    function check() {
        if (!isset($this->_check)) {
            $check_query = tep_db_query("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = 'MODULE_PAYMENT_VIABILL_QUICKPAY_STATUS'");
            $this->_check = tep_db_num_rows($check_query);
        }
        return $this->_check;
    }

    function install() {
        // add field to order table if not already there

        $cc_query = tep_db_query("describe " . TABLE_ORDERS . " cc_transactionid");
        if (tep_db_num_rows($cc_query) == 0) {
            tep_db_query("ALTER TABLE " . TABLE_ORDERS . " ADD cc_transactionid VARCHAR( 64 ) NULL default 'NULL'");
        }
       $cc_query = tep_db_query("describe " . TABLE_ORDERS . " cc_cardhash");
        if (tep_db_num_rows($cc_query) == 0) {
            tep_db_query("ALTER TABLE " . TABLE_ORDERS . " ADD cc_cardhash VARCHAR( 64 ) NULL default 'NULL'");
        }
 $cc_query = tep_db_query("describe " . TABLE_ORDERS . " cc_cardtype");
        if (tep_db_num_rows($cc_query) == 0) {
            tep_db_query("ALTER TABLE " . TABLE_ORDERS . " ADD cc_cardtype VARCHAR( 64 ) NULL default 'NULL'");
        }
        tep_db_query("ALTER TABLE  " . TABLE_ORDERS . " CHANGE  cc_expires  cc_expires VARCHAR( 8 )  NULL DEFAULT NULL");
        
    
       // new status for quickpay prepare orders
        $check_query = tep_db_query("select orders_status_id from " . TABLE_ORDERS_STATUS . " where orders_status_name = 'ViaBill-Quickpay [preparing]' limit 1");

        if (tep_db_num_rows($check_query) < 1) {
            $status_query = tep_db_query("select max(orders_status_id) as status_id from " . TABLE_ORDERS_STATUS);
            $status = tep_db_fetch_array($status_query);

            $status_id = $status['status_id'] + 1;

            $languages = tep_get_languages();

            for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                tep_db_query("insert into " . TABLE_ORDERS_STATUS . " (orders_status_id, language_id, orders_status_name) values ('" . $status_id . "', '" . $languages[$i]['id'] . "', 'ViaBill-Quickpay [preparing]')");
            }

            // compatibility ms2.2 
            $flags_query = tep_db_query("describe " . TABLE_ORDERS_STATUS . " public_flag");
            if (tep_db_num_rows($flags_query) == 1) {
                tep_db_query("update " . TABLE_ORDERS_STATUS . " set public_flag = 0 and downloads_flag = 0 where orders_status_id = '" . $status_id . "'");
            }
        } else {
            $check = tep_db_fetch_array($check_query);

            $status_id = $check['orders_status_id'];
        }


        // new status for quickpay rejected orders
        $check_query = tep_db_query("select orders_status_id from " . TABLE_ORDERS_STATUS . " where orders_status_name = 'ViaBill-Quickpay [rejected]' limit 1");

        if (tep_db_num_rows($check_query) < 1) {
            $status_query = tep_db_query("select max(orders_status_id) as status_id from " . TABLE_ORDERS_STATUS);
            $status = tep_db_fetch_array($status_query);

            $status_rejected_id = $status['status_id'] + 1;

            $languages = tep_get_languages();

            for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                tep_db_query("insert into " . TABLE_ORDERS_STATUS . " (orders_status_id, language_id, orders_status_name) values ('" . $status_rejected_id . "', '" . $languages[$i]['id'] . "', 'ViaBill-Quickpay [rejected]')");
            }

            // compatibility ms2.2 
            $flags_query = tep_db_query("describe " . TABLE_ORDERS_STATUS . " public_flag");
            if (tep_db_num_rows($flags_query) == 1) {
                tep_db_query("update " . TABLE_ORDERS_STATUS . " set public_flag = 0 and downloads_flag = 0 where orders_status_id = '" . $status_rejected_id . "'");
            }
        } else {
            $check = tep_db_fetch_array($check_query);

            $status_rejected_id = $check['orders_status_id'];
        }
        
        // new status for quickpay pending orders
        $check_query = tep_db_query("select orders_status_id from " . TABLE_ORDERS_STATUS . " where orders_status_name = 'Pending [ViaBill-Quickpay approved]' limit 1");

        if (tep_db_num_rows($check_query) < 1) {
            $status_query = tep_db_query("select max(orders_status_id) as status_id from " . TABLE_ORDERS_STATUS);
            $status = tep_db_fetch_array($status_query);

            $status_pending_id = $status['status_id'] + 1;

            $languages = tep_get_languages();

            for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                tep_db_query("insert into " . TABLE_ORDERS_STATUS . " (orders_status_id, language_id, orders_status_name) values ('" . $status_pending_id . "', '" . $languages[$i]['id'] . "', 'Pending [ViaBill-Quickpay approved]')");
            }

            // compatibility ms2.2 
            $flags_query = tep_db_query("describe " . TABLE_ORDERS_STATUS . " public_flag");
            if (tep_db_num_rows($flags_query) == 1) {
                tep_db_query("update " . TABLE_ORDERS_STATUS . " set public_flag = 0 and downloads_flag = 0 where orders_status_id = '" . $status_pending_id . "'");
            }
        } else {
            $check = tep_db_fetch_array($check_query);

            $status_pending_id = $check['orders_status_id'];
        }
        
        tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Enable viabill_quickpay', 'MODULE_PAYMENT_VIABILL_QUICKPAY_STATUS', 'False', 'Do you want to accept viabill-quickpay payments?', '6', '3', 'tep_cfg_select_option(array(\'True\', \'False\'), ', now())");
 
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Preparing orders mode', 'MODULE_PAYMENT_VIABILL_QUICKPAY_MODE', 'Normal', 'Choose  mode:<br><b>Normal:</b> Create when payment window is opened.<br><b>Before:</b> Create when confirmation page is opened', '6', '3', 'tep_cfg_select_option(array(\'Normal\', \'Before\'), ', now())");


        tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, set_function, date_added) values ('Payment Zone', 'MODULE_PAYMENT_VIABILL_QUICKPAY_ZONE', '0', 'If a zone is selected, only enable this payment method for that zone.', '6', '2', 'tep_get_zone_class_title', 'tep_cfg_pull_down_zone_classes(', now())");
       
        tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Quickpay Merchant Id', 'MODULE_PAYMENT_VIABILL_QUICKPAY_MERCHANTID', '', 'Enter Merchant id', '6', '6', now())"); 
        
    tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Quickpay Window user Agreement Id', 'MODULE_PAYMENT_VIABILL_QUICKPAY_AGGREEMENTID', '', 'Enter Window user Agreement id', '6', '6', now())");
        
        tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Order number prefix', 'MODULE_PAYMENT_VIABILL_QUICKPAY_ORDERPREFIX', '000', 'Enter prefix (Ordernumbers Must contain at least 3 characters)<br>Please Note: if upgrading from previous versions of Quickpay 10, use format \"Window Agreement ID_\" ex. 1234_ if \"old\" orders statuses  are to be displayed in your order admin.<br>', '6', '6', now())");


    //   tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Set Private key for your Quickpay Payment Gateway', 'MODULE_PAYMENT_VIABILL_QUICKPAY_PRIVATEKEY', '', 'Enter your Private key.', '6', '6', now())");
         tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('API USER KEY', 'MODULE_PAYMENT_VIABILL_QUICKPAY_USERAPIKEY', '', 'Used for payments, and for handling transactions from your backend order page.', '6', '6', now())");

         //Price Tag 
          tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('ViaBill Price Tag', 'MODULE_PAYMENT_VIABILL_QUICKPAY_PRICETAGSRC', '', 'This is the script received from ViaBill.', '6', '6', now())");



        
tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Subscription payment', 'MODULE_PAYMENT_VIABILL_QUICKPAY_SUBSCRIPTION', 'Normal', 'Set Subscription payment as default (normal is single payment).', '6', '0', 'tep_cfg_select_option(array(\'Normal\', \'Subscription\'), ',now())");

        
tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Autofee', 'MODULE_PAYMENT_VIABILL_QUICKPAY_AUTOFEE', 'No', 'Does customer pay the cardfee?<br>Set fees in <a href=\"https://manage.quickpay.net/\" target=\"_blank\"><u>Quickpay manager</u></a>', '6', '0', 'tep_cfg_select_option(array(\'Yes\', \'No\'), ',now())");

tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Autocapture', 'MODULE_PAYMENT_VIABILL_QUICKPAY_AUTOCAPTURE', 'No', 'Use autocapture?', '6', '0', 'tep_cfg_select_option(array(\'Yes\', \'No\'), ',now())");     
       
            $qp_group ='viabill';
            tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Payment Option ', 'MODULE_PAYMENT_VIABILL_QUICKPAY_GROUP', '" . $qp_group . "', 'Exclusively ViaBill Option Only ', '6', '6', now())");
      

        tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Sort order of display.', 'MODULE_PAYMENT_VIABILL_QUICKPAY_SORT_ORDER', '0', 'Sort order of display. Lowest is displayed first.', '6', '0', now())");


        // new settings
        

                        


        tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, use_function, date_added) values ('Set Preparing Order Status', 'MODULE_PAYMENT_VIABILL_QUICKPAY_PREPARE_ORDER_STATUS_ID', '" . $status_id . "', 'Set the status of prepared orders made with this payment module to this value', '6', '0', 'tep_cfg_pull_down_order_statuses(', 'tep_get_order_status_name', now())");
      
        tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, use_function, date_added) values ('Set Quickpay Acknowledged Order Status', 'MODULE_PAYMENT_VIABILL_QUICKPAY_ORDER_STATUS_ID', '" . $status_pending_id . "', 'Set the status of orders made with this payment module to this value', '6', '0', 'tep_cfg_pull_down_order_statuses(', 'tep_get_order_status_name', now())");
       
        tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, use_function, date_added) values ('Set Quickpay Rejected Order Status', 'MODULE_PAYMENT_VIABILL_QUICKPAY_REJECTED_ORDER_STATUS_ID', '" . $status_rejected_id . "', 'Set the status of rejected orders made with this payment module to this value', '6', '0', 'tep_cfg_pull_down_order_statuses(', 'tep_get_order_status_name', now())");

    }

    function remove() {
        tep_db_query("delete from " . TABLE_CONFIGURATION . " where configuration_key in ('" . implode("', '", $this->keys()) . "')");
    }

    function keys() {
        $keys = array('MODULE_PAYMENT_VIABILL_QUICKPAY_STATUS',
                    'MODULE_PAYMENT_VIABILL_QUICKPAY_ZONE',
                    'MODULE_PAYMENT_VIABILL_QUICKPAY_SORT_ORDER', 
                    'MODULE_PAYMENT_VIABILL_QUICKPAY_MERCHANTID', 
                    'MODULE_PAYMENT_VIABILL_QUICKPAY_AGGREEMENTID',
                    'MODULE_PAYMENT_VIABILL_QUICKPAY_PRICETAGSRC',
                    'MODULE_PAYMENT_VIABILL_QUICKPAY_USERAPIKEY',
                    'MODULE_PAYMENT_VIABILL_QUICKPAY_ORDERPREFIX',
                    'MODULE_PAYMENT_VIABILL_QUICKPAY_PREPARE_ORDER_STATUS_ID', 
                    'MODULE_PAYMENT_VIABILL_QUICKPAY_ORDER_STATUS_ID', 
                    'MODULE_PAYMENT_VIABILL_QUICKPAY_REJECTED_ORDER_STATUS_ID',
                    'MODULE_PAYMENT_VIABILL_QUICKPAY_SUBSCRIPTION',
                    'MODULE_PAYMENT_VIABILL_QUICKPAY_AUTOFEE',
                    'MODULE_PAYMENT_VIABILL_QUICKPAY_AUTOCAPTURE',
                    'MODULE_PAYMENT_VIABILL_QUICKPAY_MODE'                   
                    );
        
        

        return $keys;
    }

//------------- Internal help functions-------------------------
// $order_total parameter must be total amount for current order including tax
// format of $fee parameter: "[fixed fee]:[percentage fee]"
    function calculate_order_fee($order_total, $fee) {
        list($fixed_fee, $percent_fee) = explode(':', $fee);
        
        
        return ((float) $fixed_fee + (float) $order_total * ($percent_fee / 100));
    }

    function get_order_fee() {
        global $_POST, $order, $currencies, $vbquickpay_fee;
        $vbquickpay_fee = 0.0;
        if (isset($_POST['vbqp_card']) && strpos($_POST['vbqp_card'], ":")) {
            $vbquickpay_fee = $this->calculate_order_fee($order->info['total'], $_POST['vbqp_card']);
        }
    }

    function get_payment_options_name($payment_option) {
        switch ($payment_option) {
           
            case 'viabill':  return MODULE_PAYMENT_VIABILL_QUICKPAY_IBILL_DESCRIPTION;
            
        }
        return '';
    }
public function sign($params, $api_key) {
    ksort($params);
   $base = implode(" ", $params);
 
   return hash_hmac("sha256", $base, $api_key);
 }
 
private function get_vbquickpay_order_status($order_id,$mode="") {

    $api= new ViaBillQuickpayApi();
    

    $api->setOptions(MODULE_PAYMENT_VIABILL_QUICKPAY_USERAPIKEY);

  try {
    $api->mode = ($mode=="" ? "payments?order_id=" : "subscriptions?order_id=");
    

    // Commit the status request, checking valid transaction id
    $st = $api->status(MODULE_PAYMENT_VIABILL_QUICKPAY_ORDERPREFIX.sprintf('%04d', $order_id));
        $eval = array();
    if($st[0]["id"]){

    $eval["oid"] = str_replace(MODULE_PAYMENT_VIABILL_QUICKPAY_ORDERPREFIX,"", $st[0]["order_id"]);
    $eval["qid"] = $st[0]["id"];
    }else{
    $eval["oid"] = null;
    $eval["qid"] = null;    
    }
  
  } catch (Exception $e) {
   $eval = 'ViaBill-QuickPay Status: ';
            // An error occured with the status request
          $eval .= 'Problem: ' . $this->json_message_front($e->getMessage()) ;
         //  tep_redirect(tep_href_link(FILENAME_CHECKOUT_PAYMENT, 'payment_error=' . $this->code, 'SSL'));
  }

    return $eval;
  } 

private function json_message_front($input){
    
    $dec = json_decode($input,true);
    
    $message= $dec["message"];

    return $message;
    
    
}
}

/*$paiioptions = array();
    $options = '';  
    $paiique = tep_db_query("select configuration_value  from ".TABLE_CONFIGURATION. " WHERE configuration_key  =  'MODULE_PAYMENT_VIABILL_QUICKPAY_PAII_CAT' ");
    $paiicat_values = tep_db_fetch_array($paiique);
    $selectedcat = $paiicat_values['configuration_value'];*/

    $option_array=array();  
/*foreach($paiioptions as $arrid => $val){
     $option_array[] = array('id' => $arrid,
                              'text' => $val);
     $selected ='';
      if ($selectedcat == $arrid) {
        $selected = ' selected="selected"';
      }                         
     $options .= '<option value="'.$arrid.'" '.$selected.' >'.$val.'</option>';
}*/

  
 /* function tep_cfg_pull_down_paii_list($option_array) {
     global $options;
    return "<select name='configuration[MODULE_PAYMENT_VIABILL_QUICKPAY_PAII_CAT]' />
    ".$options."    
    </select>";

  }*/



?>